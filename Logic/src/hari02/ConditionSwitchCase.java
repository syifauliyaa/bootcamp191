package hari02;

public class ConditionSwitchCase {
	public static void main(String[] args) {
		//cahr grade = args
		char grade = 'A';
		switch (grade) {
		case 'A':
			System.out.println("Excellent");
			break;
		case 'B':
		case 'C':
			System.out.println("Well Done");
			break;
		case 'D':
			System.out.println("You Passed");
		case 'E':
			System.out.println("Better Try Again");
			break;
		default:
			System.out.println("Invalid Grade");
		}
		System.out.println("Your Grade is " + grade);
	}

}
