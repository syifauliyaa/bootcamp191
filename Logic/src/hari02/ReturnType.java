package hari02;

public class ReturnType {
	
	public static void main(String[] args) {
		int a = 11;
		int b = 6;
		//calling method minFunction
		//with parameter variable a and b
		//variable a b must be same data
		//with parameter at method minFunction
		int c = minFunction(a,b);
		System.out.println("Minimum value = " + c);
	}
	
	/** return the minimum of two numbers */
	public static int minFunction(int n1, int n2) {
		int min;
		if (n1> 2)
			min = n2;
		else
			min = n1;
		
		return min;
		}
}