package hari02;

public class ConditionIfElse {
	
	public static void main(String[] args) {
		//initial var x int type data
		int x = 10;
		//condition if value of variable x less then 20
		if (x < 20) {
			System.out.print("This is if x value less then 20");
		} else {
			System.out.println("This is x value more and equal 20");
		}
	}
}