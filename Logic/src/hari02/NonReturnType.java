package hari02;

public class NonReturnType {

		public static void main(String[] args) {
			// initial varibale number
			double number = 255.7;
			// call methodRankPOints
			methodRankPoints(number);
		}
		// rank point
		public static void methodRankPoints(double points) {
			if (points >= 202.5) {
				System.out.println("Rank:A1");
			} else if (points >= 122.4) {
				System.out.println("Rank:A2");
			} else {
				System.out.println("Rank:A3");
			}
		}
}
