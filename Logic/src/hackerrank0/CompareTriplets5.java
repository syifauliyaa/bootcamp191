package hackerrank0;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CompareTriplets5 {

	static List<Integer> compareTriplets5(List<Integer> j, List<Integer> k) {
		
		List<Integer> result = new ArrayList();
		
		result.add(0);
		result.add(0);
		
		int nIca = 0;
		int nAci = 0;
		
		for (int i = 0; i < j.size(); i++) {
			if(j.get(i) > k.get(i)) {
				nIca++;
				result.set(0, nIca);
			}
			if(j.get(i) < k.get(i)) {
				nAci++;
				result.set(1, nAci);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws IOException {
		
		List<Integer> j = new ArrayList<Integer>();
			j.add(17);
			j.add(28);
			j.add(30);
		
		List<Integer> k = new ArrayList<Integer>();
			k.add(29);
			k.add(16);
			k.add(20);
		
		for(Integer item : compareTriplets5(j, k)) {
			System.out.print(item + "\t");
		}
	}
}
