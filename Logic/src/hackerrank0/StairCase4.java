package hackerrank0;

	
	public class StairCase4 {
		
		public static void main(String[] args) {
			
			
			int m=6;
			
			for (int i = 0; i < m; i++) {
				for (int j = 0; j <= m-i-2; j++) {
					System.out.print(" ");
				}
				for (int j = m-i-1; j < m; j++) {
					System.out.print("&");
				}
				System.out.println();
			}
		}
	
	}
