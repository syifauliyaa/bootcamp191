package hackerrank0;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CompareTriplets4 {
	
	static List<Integer> compareTriplets4(List<Integer> m, List<Integer> n) {
		
		List<Integer> result = new ArrayList();
		
		result.add(0);
		result.add(0);
		
		int nIda=0;
		int nAdi=0;
		
		for(int i = 0; i < m.size(); i++) {
			if (m.get(i) > n.get(i)) {
				nIda++;
				result.set(0, nIda);
			}
			if (m.get(i) < n.get(i)) {
				nAdi++;
				result.set(1, nAdi);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws IOException{
		
		List<Integer> m = new ArrayList<Integer>();
			m.add(17);
			m.add(28);
			m.add(30);
			
		List<Integer> n = new ArrayList<Integer>();
			n.add(29);
			n.add(16);
			n.add(20);
		
		for (Integer item : compareTriplets4(m, n)) {
			System.out.print(item + "\t");
		}
	}

}
