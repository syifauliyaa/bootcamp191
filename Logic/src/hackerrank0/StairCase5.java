package hackerrank0;

	public class StairCase5 {
		
		public static void main(String[] args) {
			
			int o=6;
			
			for (int i = 0; i < o; i++) {
				for (int j = 0; j <= o-i-2; j++) {
					System.out.print(" ");
				}
				for (int j = o-i-1; j < o; j++) {
					System.out.print("%");
				}
				System.out.println();
			}
		}
	
	}
