package hackerrank0;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CompareTriplets3 {
	
	static List<Integer> compareTriplets3(List<Integer> e, List<Integer> f) {
		
		List<Integer> result = new ArrayList();
		
		result.add(0);
		result.add(0);
		
		int nAri=0;
		int nIra=0;
		
		for(int i = 0; i < e.size(); i++) {
			if (e.get(i) > f.get(i)) {
				nAri++;
				result.set(0, nAri);
			}
			if (e.get(i) < f.get(i)) {
				nIra++;
				result.set(1, nIra);
			}
		}
			return result;
	}
		public static void main(String[] args) throws IOException {
			List<Integer> e = new ArrayList<Integer>();
			e.add(17);
			e.add(28);
			e.add(30);
			
			List<Integer> f = new ArrayList<Integer>();
			f.add(99);
			f.add(16);
			f.add(20);
			
			for (Integer item : compareTriplets3(e, f)) {
				System.out.print(item + "\t");
			}
		}

}
