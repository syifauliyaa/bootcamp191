package hackerrank0;

public class PlusMinus2 {
		
	static void plusMinus2(int[] arr2) {
		
		float d=0;
		float e=0;
		float f=0;
		
		for (int i = 0; i < arr2.length; i++) {
			if(arr2[i] > 0) {
				d++;
			} else if (arr2[i] < 0) {
				e++;
			} else if (arr2[i] == 0) {
				f++;
			}
		}
		System.out.println(d / arr2.length);
		System.out.println(e / arr2.length);
		System.out.println(f / arr2.length);
	}
	
	public static void main(String[] args) {
		int[] arr2 = new int[] {-5, 1, -10, 1, 4, 2};
		plusMinus2(arr2);
	}
}