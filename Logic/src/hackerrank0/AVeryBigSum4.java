package hackerrank0;

public class AVeryBigSum4 {

	//method return
		static long aVeryBigSum4(long[] sum) {
			int d=0;
			for (int i = 0; i < sum.length; i++) {
				d+=sum[i];
			}
			return d;
		}
		//input program
		public static void main(String[] args) {	//method non return
			long[] sum = new long[] {100011, 100012, 100013, 100014, 100015};
			long hasil = aVeryBigSum4(sum);
			System.out.println(hasil);
		}
}
