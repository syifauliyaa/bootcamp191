package hackerrank0;

public class AVeryBigSum5 {

	//method return
		static long aVeryBigSum5(long[] summ) {
			int e=0;
			for (int i = 0; i < summ.length; i++) {
				e+=summ[i];
			}
			return e;
		}
		
		//input
		public static void main(String[] args) { // method non return
			
			long[] summ = new long[] {201, 202, 203, 204, 205};
			long jumlah = aVeryBigSum5(summ);
			System.out.println(jumlah);
			
		}
}
