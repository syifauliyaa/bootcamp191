package hackerrank0;

public class AVeryBigSum {
	
	//method return
		static long aVeryBigSum(long[] ar) {
			int a=0;
			for (int i = 0; i < ar.length; i++) {
				a+=ar[i];
			}
			return a;
		}
		
		//input program
		public static void main(String[] args) {
			
			long[] ar = new long[] {1000001, 1000002, 1000003, 1000004, 1000005};
			long hasil = aVeryBigSum(ar);
			System.out.println(hasil);
			
		}
}
