package hackerrank0;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CompareTriplets2 {
	
		static List<Integer> compareTriplets2(List<Integer> c, List<Integer> d) {
			
			List<Integer> result = new ArrayList();
			
			result.add(0);
			result.add(0);
			
			int nRose = 0;
			int nRoy = 0;
			
			for (int i=0; i < c.size(); i++) {
				if(c.get(i) > d.get(i)) {
					nRose++;
					result.set(0, nRose);
				}
				if(c.get(i) < d.get(i)) {
					nRoy++;
					result.set(1, nRoy);
				}
			}
			return result;
		}
			
			public static void main(String[] args) throws IOException {
				List<Integer> c = new ArrayList<Integer>();
				c.add(17);
				c.add(28);
				c.add(30);
				
				List<Integer> d = new ArrayList<Integer>();
				d.add(99);
				d.add(16);
				d.add(20);
				
				for(Integer item : compareTriplets2(c, d)) {
					System.out.print(item + "\t");
				}
					
				
			}

}
