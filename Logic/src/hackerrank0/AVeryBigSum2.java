package hackerrank0;

public class AVeryBigSum2 {
	
	//method return
	static long aVeryBigSum2(long[] arr) {
		long b=0;
		for (int i = 0; i < arr.length; i++) {
			b+=arr[i];
		}
		return b;
	}
	//input program
	public static void main(String[] args) { //method non return
		
		long[] arr = new long[] {1001, 1002, 1003, 1004, 1005};
		long jumlah = aVeryBigSum2(arr); //tipe data long
		System.out.println(jumlah);
	}
}
