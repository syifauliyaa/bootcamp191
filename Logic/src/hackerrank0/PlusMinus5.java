package hackerrank0;

public class PlusMinus5 {
	
	static void plusMinus5(int[] arr5) {
		
		float r=0;
		float s=0;
		float t=0;
		
		for (int i = 0; i < arr5.length; i++) {
			if(arr5[i] > 0) {
				r++;
			} else if(arr5[i] < 0) {
				s++;
			} else if(arr5[i] == 0) {
				t++;
			}
		}
		System.out.println(r / arr5.length);
		System.out.println(s / arr5.length);
		System.out.println(t / arr5.length);
	}
	
	public static void main(String[] args) {
		int[] arr5 = new int[] {-8, 4, -13, 4, 7, 5};
		plusMinus5(arr5);
	}

}
