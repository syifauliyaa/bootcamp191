package hackerrank0;

	public class StairCase {
		
		public static void main(String[] args) {
			
			int a=6;
			
			for (int i = 0; i < a; i++) {
				for (int j = 0; j <= a-i-2; j++) {
					System.out.print(" ");
				}
				for (int j = a-i-1; j < a; j++) {
					System.out.print("@");
				}
				System.out.println();
			}
		}
	
	}
