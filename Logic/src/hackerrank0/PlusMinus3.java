package hackerrank0;

public class PlusMinus3 {
	
	static void plusMinus3(int[] arr3) {
		
		float j=0;
		float k=0;
		float l=0;
		
		for (int i = 0; i < arr3.length; i++) {
			if (arr3[i] > 0) {
				j++;
			} else if (arr3[i] < 0) {
				k++;
			} else if (arr3[i] == 0) {
				l++;
			}
		}
		System.out.println(j / arr3.length);
		System.out.println(k / arr3.length);
		System.out.println(l / arr3.length);
	}
	
	public static void main(String[] args) {
		int[] arr3 = new int[] {-6, 2, -11, 2, 5, 3};
		plusMinus3(arr3);
	}

}
