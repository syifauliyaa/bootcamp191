package hackerrank0;

public class PlusMinus4 {
	
	static void plusMinus4(int[] arr4) {
		
		float m=0;
		float n=0;
		float o=0;
		
		for (int i = 0; i < arr4.length; i++) {
			if(arr4[i] > 0) {
				m++;
			} else if (arr4[i] < 0) {
				n++;
			} else if (arr4[i] == 0) {
				o++;
			}
		}
		System.out.println(m / arr4.length);
		System.out.println(n / arr4.length);
		System.out.println(o / arr4.length);
	}
	
	public static void main(String[] args) {
		int[] arr4 = new int[] {-7, 3, -12, 3, 6 , 4};
		plusMinus4(arr4);
	}
}
