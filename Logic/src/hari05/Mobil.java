package hari05;

public class Mobil {
	
	//property
	int no;
	String merek;
	String nama;
	String jenis;
	int tahun;
	
	//construktor 1
	public Mobil() {
		
	}
	
	//constuktor 2
	public Mobil(int no, String merek, String nama, String jenis, int tahun) {
		this.no = no;
		this.merek = merek;
		this.nama = nama;
		this.jenis = jenis;
		this.tahun = tahun;
	}
	
	public void showData() {
		System.out.println("Nomor \t: " +this.no);
		System.out.println("Merek \t: " +this.merek);
		System.out.println("Nama \t: " +this.nama);
		System.out.println("Jenis \t: " +this.jenis);
		System.out.println("Tahun \t: " +this.tahun);
	}
}
