package hari05;

public class OrangMain {
	
	public static void main(String[] args) {
		Orang org1 = new Orang(1, "Syifa", "Depok", "Wanita", 22);
		System.out.println("Data Orang 1:");
		org1.showData();
		
		System.out.println(" ");
		
		Orang org2 = new Orang(2, "Sela", "Depok", "Wanita", 20);
		System.out.println("Data orang 2:");
		org2.showData();
	}

}
