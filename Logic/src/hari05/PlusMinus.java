package hari05;


import java.util.*;


public class PlusMinus {

	static void plusMinus(int[] arr) {
        
		float a=0;
        float b=0;
        float c=0;

        for(int i=0; i < arr.length; i++) {
            if(arr[i] > 0) {
                a++;
            } else if (arr[i] < 0) {
                b++;
            } else if (arr[i]==0) {
                c++;
            }
        }
        System.out.println(a / arr.length);
        System.out.println(b / arr.length);
        System.out.println(c / arr.length);
		
    }
	
	public static void main(String[] args) {
		int[] arr = new int[] {-4, 3, -9, 0, 4, 1};
		plusMinus(arr);
	}
}
