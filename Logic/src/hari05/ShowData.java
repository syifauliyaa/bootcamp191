package hari05;

public class ShowData {

	public static void main(String[] args) {
			
			Motor mtr1 = new Motor(1, "Honda", "Vario", 2015, "Matic");
			System.out.println("Data Motor 1 :");
			mtr1.showData();
			
			System.out.println(" ");
			
			Motor mtr2 = new Motor(2, "Honda", "Blade", 2016, "Manual");
			System.out.println("Data Motor 2 :");
			mtr2.showData();
			
			System.out.println(" ");
			
			Motor mtr3 = new Motor(3, "Suzuki", "Spin", 2018, "Matic");
			System.out.println("Data Motor 3 :");
			mtr3.showData();
			
			System.out.println(" ");
			
			Motor mtr4 = mtr1;
			System.out.println("Data Motor 4 :");
			mtr1.nama="Beat";
			mtr4.showData();
			
			System.out.println(" ");
			
			System.out.println("Data Motor 1 :");
			mtr1.showData();
			
			System.out.println("=====================================");
			
			Mobil mbl1 = new Mobil(1, "Toyota", "Avanza", "Family Car", 2015);
			System.out.println("Data Mobil 1 : ");
			mbl1.showData();
			
			System.out.println(" ");
			
			Mobil mbl2 = new Mobil(2, "Honda", "Hrv", "City Car", 2018);
			System.out.println("Data Mobil 2 : ");
			mbl2.showData();
			
			System.out.println(" ");
			
			Mobil mbl3 = new Mobil(3, "Suzuki", "Ertiga", "Family Car", 2017);
			System.out.println("Data Mobil 3 : ");
			mbl3.showData();
			
			System.out.println(" ");
			
			Mobil mbl4 = mbl2;
			System.out.println("Data Mobil 4 :");
			mbl2.jenis="Family Car";
			mbl4.showData();
			
			System.out.println(" ");
			
			System.out.println("Data Mobil 2 : ");
			mbl2.showData();
			
			System.out.println("=========================================");
			
			Kantor ktr1 = new Kantor(123, "Dina", "PT visi", "IT", 2015);
			System.out.println("Data Kantor 1 : ");
			ktr1.showData();
			
			System.out.println(" ");
			
			Kantor ktr2 = new Kantor(112, "Rafi", "Pt Visi", "Marketing", 2017);
			System.out.println("Data Kantor 2 : ");
			ktr2.showData();
			
			System.out.println(" ");
			
			Kantor ktr3 = new Kantor(133, "Dono", "Pt Visi", "Teknisi", 2013);
			System.out.println("Data Kantor 3 : ");
			ktr3.showData();
			
			System.out.println(" ");
			
			Kantor ktr4 = ktr1;
			System.out.println("Data Kantor 4 :");
			ktr4.nama="Roy";
			ktr1.showData();
			
			System.out.println(" ");
			
			System.out.println("Data Kantor 1 : ");
			ktr1.showData();
			
			System.out.println("=========================================");
			
			Sekolah skl1 = new Sekolah(145, "Sari", "SMA 7", "IPA", 1);
			System.out.println("Data Sekolah 1 : ");
			skl1.showData();
			
			System.out.println(" ");
			
			Sekolah skl2 = new Sekolah(133, "Panji", "SMA 7", "IPS", 2);
			System.out.println("Data Sekolah 2 : ");
			skl2.showData();
			
			System.out.println(" ");
			
			Sekolah skl3 = new Sekolah(111, "indah", "SMA 7", "Bahasa", 3);
			System.out.println("Data Sekolah 3 : ");
			skl3.showData();
			
			System.out.println(" ");
			
			Sekolah skl4 = skl3;
			System.out.println("Data Sekolah 4 : ");
			skl3.nama="Tuti";
			skl4.showData();
			
			System.out.println(" ");
			
			System.out.println("Data Sekolah 3 : ");
			skl3.showData();
	}
}
