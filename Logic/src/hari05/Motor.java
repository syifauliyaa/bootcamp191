package hari05;

public class Motor {
	
	//property
	int no;
	String merek;
	String nama;
	int tahun;
	String jenis;
	
	//construktor
	public Motor() {
	}

	//contruktor2
	public Motor(int no, String merek, String nama, int tahun, String jenis) {
		this.no = no;
		this.merek = merek;
		this.nama = nama;
		this.tahun = tahun;
		this.jenis = jenis;
	}
	
	public void showData() {
		System.out.println("Nomor \t: " +this.no);
		System.out.println("Merek \t: " +this.merek);
		System.out.println("Nama \t: " +this.nama);
		System.out.println("Tahun \t: " +this.tahun);
		System.out.println("Jenis \t: " +this.jenis);
	}
}
