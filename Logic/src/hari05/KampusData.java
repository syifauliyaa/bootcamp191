package hari05;

public class KampusData {
	
	public static void main(String[] args) {
		Kampus kps1 = new Kampus(12345, "Syifa", 113, "Si", "Gundar");
		System.out.println("Data Mahasiswa 1:");
		kps1.showKampus();
		
		System.out.println(" ");
		
		Kampus kps2 = new Kampus(12123, "Seila", 115, "Mi", "Gundar");
		System.out.println("Data Mahasiswa 2:");
		kps2.showKampus();
		
		System.out.println(" ");
		
		Kampus kps3 = kps1;
		System.out.println("Data Mahasiswa 3:");
		kps1.nama = "Hana";
		kps3.showKampus();
		
		System.out.println(" ");
		
		System.out.println("Data Mahasiswa 1:");
		kps1.showKampus();
	}

}
