package hari05;

public class Kantor {
	
	//property
	int nik;
	String nama;
	String kantor;
	String divisi;
	int tahun;
	
	//construktor
	public Kantor() {
	}
	
	//construktor
	public Kantor(int nik, String nama, String kantor, String divisi, int tahun) {
		this.nik = nik;
		this.nama = nama;
		this.kantor = kantor;
		this.divisi = divisi;
		this.tahun = tahun;
	}
	
	public void showData() {
		System.out.println("Nomor \t: " +this.nik);
		System.out.println("Merek \t: " +this.nama);
		System.out.println("Nama \t: " +this.kantor);
		System.out.println("Jenis \t: " +this.divisi);
		System.out.println("Tahun \t: " +this.tahun);
	}
}
