package hari05;

public class Kampus {
	
	//property
	int npm;
	String nama;
	int kelas;
	String jurusan;
	String kampus;
	
	//contruktor
	public Kampus() {
	}
	
	//contruktor2
	public Kampus(int npm, String nama, int kelas, String jurusan, String kampus) {
		this.npm = npm;
		this.nama = nama;
		this.kelas = kelas;
		this.jurusan = jurusan;
		this.kampus = kampus;
	}

	public void showKampus() {
		// TODO Auto-generated method stub
		System.out.println("Npm \t:" +this.npm);
		System.out.println("Nama \t:" +this.nama);
		System.out.println("Kelas \t:" +this.kelas);
		System.out.println("Jurusan :" +this.jurusan);
		System.out.println("Kampus \t:" +this.kampus);
	}
}
