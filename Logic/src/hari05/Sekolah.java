package hari05;

public class Sekolah {
	
	//property
	int nis;
	String nama;
	String sekolah;
	String jurusan;
	int kelas;
	
	//construktor
	public Sekolah() {
	}
	
	//construktor2
	public Sekolah(int nis, String nama, String sekolah,  String jurusan, int kelas) {
		this.nis = nis;
		this.nama = nama;
		this.sekolah =sekolah;
		this.jurusan = jurusan;
		this.kelas = kelas;
	}
	
	public void showData() {
		System.out.println("NIS \t: " +this.nis);
		System.out.println("Nama \t: " +this.nama);
		System.out.println("Sekolah : " +this.sekolah);
		System.out.println("Jurusan : " +this.jurusan);
		System.out.println("Kelas \t: " +this.kelas);
	}
}
