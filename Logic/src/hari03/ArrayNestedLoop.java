package hari03;

public class ArrayNestedLoop {
	
	public static void main(String[] args) {
		
		int[][] array = new int[9][9]; 
		int number = 1;
		for(int i=0; i < array.length; i++){
		  for(int j=0; j < array[i].length; j++){
		    array[i][j]=number;
		  }
		  number = number + 3;
		}
		
		System.out.println(" hitung =" +number);
	}

}
