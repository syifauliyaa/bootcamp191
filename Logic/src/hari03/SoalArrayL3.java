package hari03;

import java.util.Scanner;

public class SoalArrayL3 {
	
	static Scanner scn;
	
	public static void main(String[] args) {
		
		scn = new Scanner(System.in);
		
		// input nilai m n o
		System.out.print("Masukan N =");
		int n = scn.nextInt();
		System.out.print("Masukan M =");
		int m = scn.nextInt();
		System.out.print("Masukan O =");
		int o =scn.nextInt();
		
		//1. buat array deret
		int[] deret = new int[n*4];
		int angka = o;
		
		for (int i = 0; i < deret.length; i++) {
			if (i % 4 == 3) {
				deret[i]=m;
			} else {
				deret[i]=angka;
				angka = angka+m;
			}
		}
		//2. buat array 2 dimensi
		String[][] array = new String[n][n];
		//4. membuat index
		int index=0;
		//3. isi baris ke 0
		for (int i = 0; i < n; i++) {
			array[0][i]=deret[index]+"";
			index++;
		}
		//5. isi kolom n-1
		for (int i = 1; i < n; i++) {
			array[i][n-1]=deret[index]+"";
			index++;
		}
		//6. isi baris ke n-1
		for (int i = n-2; i >= 0; i--) {
			array[n-1][i]=deret[index]+"";
			index++;
		}
		//7. isi kolom 0
		for (int i = n-2; i>0; i--) {
			array[i][0]=deret[index]+"";
			index++;
		}
		//menampilkan
		for (int a = 0; a < array.length; a++) {
			// print dari kiri ke kanan
			for (int b = 0; b < array.length; b++) {
				System.out.print(array[a][b]+"\t");
			}
			System.out.println("\n");
		}
	}
		
}
