package hari03;

import java.util.Scanner;

public class ArrayDeret2 {
	
	static Scanner scn;
	
	public static void main(String[] args) {
		
		scn = new Scanner(System.in);
		System.out.println("Masukan Nilai :");
		int n = scn.nextInt();
		
		int[]array = new int[n];
		
		int angka1 = 3;
		int angka2 = 1;
		
		for (int i = 0; i < array.length; i++) {
			if (i % 2 == 0) {
				array[i]=angka1;
				angka1 = angka1 + 6;
			} else {
				array[i]=angka2;
				angka2 = angka2 + 2;
			}
			
			System.out.print(array[i]+"\t");
		}
	}

}
