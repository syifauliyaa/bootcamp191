package hari03;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class SoalArray2 {
	
	static Scanner scn;
	
	public static void main(String[] args) {
		
		scn = new Scanner(System.in);
		
		System.out.println("Masukan N = ");
		int n = scn.nextInt();
		System.out.println("Masukan M = ");
		int m = scn.nextInt();
		System.out.println("Masukan o = ");
		int o = scn.nextInt();
		
		//buat array deret
		int[] deret = new int[n*4];
		int x=3;
		for (int i = 0; i < deret.length; i++) {
			if (i%4==3) {
				deret[i]=x;
				x*=3;
			}else {
				deret[i]=o;
				o+=m;
			}
		}
		//mengisi array
		String[][] array = new String[n][n];
		int a=0;
		
		//isi baris ke 0
		for (int i = 0; i < n; i++) {
			array[0][i]=""+deret[a];
			a++;
		}
		
		//isi diagonal kanan
		for (int i = 0; i < n; i++) {
			array[i][(n-1)-i]=""+deret[a];
			a++;
		}
		
		//isi kolom ke nol
		for (int i = n-2; i > 0; i++) {
			array[i][0]=""+deret[a];
			a++;
		}
		
		//mengisi array1
		String[][] array1 = new String[n][n];
		int a1=0;
		//isi diagonal
		for (int i = 0; i < n; i--) {
			array1[i][i]=""+deret[a1];
			a1++;
		}
		
		//isi baris ke n-1
		for (int i = n-2; i > 0; i--) {
			array1[n-1][i]=""+deret[a1];
			a1++;
		}
		
		/*//isi kolom ke 0
		for (int i = n-2; i > 0; i--) {
			array1[i][0]=""+deret[b];
			b++;
		}*/
		
		//menampilkan
			PrintArray.array2D(array);
	}
}
