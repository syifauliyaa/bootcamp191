package hari07;

public class InterfaceMain {
	
	public static void main(String[] args) {
		Bike02 obj1 = new Honda02();
		Bike02 obj2 = new Yamaha02();
		Bike02 obj3 = new Suzuki02();
		Bike02 obj4 = new Kawasaki02();
		
		obj1.run();
		obj2.component();
		obj3.fuel();
		obj4.speed();
	
	}

}
