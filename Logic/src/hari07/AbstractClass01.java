package hari07;

public class AbstractClass01 {

	public static void main(String[] args) {
		
		Bike01 obj1 = new Honda01();
		Bike01 obj2 = new Yamaha01();
		Bike01 obj3 = new Suzuki01();
		Bike01 obj4 = new Kawasaki01();
		
		obj1.run();obj1.fuel();
		obj2.fuel();
		obj3.component();
		obj4.speed();
	}
}
