package hari01;

public class HelloWorld {

	private static int batch;
	
	public static void main(String[] args) {
		System.out.println("Welcome to Java");
		makananfavorite();
		sampleobject();
	}
	
	public static void sampleobject() {
		
		int anu = 10;
		Orang or01 = new Orang();
		or01.nama="Via";
		or01.alamat="Magelang";
		or01.jk="Wanita";
		or01.tptlahir="Jakarta";
		or01.umur=22;
		or01.cetak();
	
		
		Orang or02 = new Orang();
		or02.nama="Syifa";
		or02.alamat="Magelang";
		or02.jk="Wanita";
		or02.tptlahir="Jakarta";
		or02.umur=22;
		or02.cetak();
		
		Orang or03 = new Orang();
		or03.nama="Budi";
		or03.alamat="Magelang";
		or03.jk="Pria";
		or03.tptlahir="Jakarta";
		or03.umur=25;
		or03.cetak();
		
		Orang or04 = new Orang();
		or04.nama="Bowo";
		or04.alamat="Magelang";
		or04.jk="Pria";
		or04.tptlahir="Jakarta";
		or04.umur=25;
		or04.cetak();
		
		Orang or05 = new Orang();
		or05.nama="Roy";
		or05.alamat="Magelang";
		or05.jk="Pria";
		or05.tptlahir="Jakarta";
		or05.umur=25;
		or05.cetak();
		
	}

	public static void makananfavorite() {
		System.out.println("1. Sate");
		System.out.println("2. Ayam");
		System.out.println("3. Bakso");
		System.out.println("4. Soto");
		System.out.println("5. Mie");
	}
 }
