package hari01;

import java.util.Scanner;

public class Looping {
	//membuat objek scn
	static Scanner scn;
	
	// method main
	public static void main(String[] args) {
		//instalasi
		// 3 1 9 15 5 21 7
		scn = new Scanner(System.in);
		System.out.print("Masukan Nilai n :");
		int n = scn.nextInt();
		int a = 3; int b=1;
		for (int i = 0; i < n; i++) {
			System.out.println(a);
			System.out.println(b);
			a+=6; b+=2;
		}
	}
	
	static int tambah(int x, int y) {
		return x+y;
	}
	
	static int kali(int m, int n) {
		return m*n;
	}
	
	static float bagi(int o, int p) {
		return o/p;
	}
	
	static int kurang(int s, int d) {
		return s-d;
	}
	
	static void inputOutput() {
		// tampilkan di layar
		System.out.println("Masukan Nama : ");
		// membuat variable
		String nama = scn.nextLine();
		// menampilkan variable dilayar
		System.out.println("Nama : "+ nama);
	}
}
