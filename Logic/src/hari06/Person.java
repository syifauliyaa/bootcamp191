package hari06;

public class Person {
	
	//property
	public int id;
	public String nama;
	public String address;
	public String gender;
	
	//contruktor
	public Person() {
	}
	
	//construktor with parameter
	public Person(int id, String nama, String address,
			String gender, String major, Double grade) {
		this.id= id;
		this.nama= nama;
		this.address= address;
		this.gender= gender;
	}
	
	//menampilkan
	private void showAll() {
		System.out.println("ID \t : " +this.id);
		System.out.println("Nama \t : " +this.nama);
		System.out.println("Address \t : " +this.address);
		System.out.println("Gender \t : " +this.gender);
	}
}
