package hari06;

public class Dosen extends Person {
	
	//property
	public String matkul;
	public String fakultas;
	
	//construktor
	public Dosen() {
		super();
	}
	
	//construktor
	public Dosen(int id, String nama, String address, String gender,
			String matkul, String fakultas) {
			super();
			this.matkul = matkul;
			this.fakultas = fakultas;
	}
	
	private void showAll() {
		System.out.println("ID \t : " +this.id);
		System.out.println("Nama \t : " +this.nama);
		System.out.println("Address \t : " +this.address);
		System.out.println("gender \t : " +this.gender);
		System.out.println("MataKuliah : " +this.matkul);
		System.out.println("Fakultas \t : " +this.fakultas);
	}
}
