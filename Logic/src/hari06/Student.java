package hari06;

public class Student extends Person {
	
	//property
	public String major;
	public Double grade;
	
	//cunstruktor
	public Student() {
		super();
	}
	
	//construktor with parameter
	public Student(int id, String nama, String address, String gender, String major, Double grade) {
		super(id, nama, address, gender, major, grade);
		this.major = major;
		this.grade = grade;
	}
	
	//menampilkan
	public void showAll() {
		System.out.println("ID \t : " +this.id);
		System.out.println("Nama \t : " +this.nama);
		System.out.println("Address \t : " +this.address);
		System.out.println("Gender \t : " +this.gender);
		System.out.println("Major \t : " +this.major);
		System.out.println("Grade \t : " +this.grade);
	}
}
